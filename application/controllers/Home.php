<?php
	class Home extends CI_Controller
	{
		public function index()
		{
			$data['query'] = $this->UserModel->GetUser();
			$this->load->view('index', $data);
		}
		public function AddUser()
		{
			$this->load->view('AddUsers');
		}
		public function PostUser()
		{
			$name = $this->input->post('nama');
			$alamat = $this->input->post('alamat');
			if ($this->input->post('submit'))
			{
				$data = array(
					'NAMA' => $name,
					'ALAMAT'=>$alamat
					);
				$this->UserModel->PostUser($data);
				redirect('Home','index');
			}
		}
	
		public function UpdateUser($nama)
		{
			$data['query'] = $this->UserModel->GetUserByNama($nama);
			$this->load->view('UpdateUsers', $data);
		}
		public function PutUser()
		{
			$name1 = $this->input->post('namabefore');
			$name = $this->input->post('nama');
			$alamat = $this->input->post('alamat');
			if ($this->input->post('submit'))
			{
				$data = array(
					'NAMA' => $name,
					'ALAMAT'=>$alamat
					);
				$this->UserModel->PutUser($name1,$data);
				redirect('Home','index');
			}
		}
		public function DeleteUser()
		{
			$this->UserModel->DelUser();
			redirect('Home','index');
		}
	}
?>