<?php
	/**
	 * 
	 */
	class UserModel extends CI_Model
	{
		public function GetUser()
		{
			$data = $this->db->get('Pegawai');
			return $data->result_array();
		}
		public function GetUserByNama($nama)
		{
			$query = $this->db->get_where('Pegawai', array('NAMA' => $nama));
			return $query->row();
		}
		public function PostUser($data)
		{
			$this->db->insert('pegawai', $data);
		}
		public function PutUser($nama,$data)
		{
			$this->db->where('NAMA', $nama)->update('Pegawai', $data);
		}
		public function DelUser()
		{
			$this->db->delete('pegawai');
		}
	}
?>