<!DOCTYPE html>
<html>
<head>
	<title>User List</title>
</head>
<body>
	<a href="<?php echo base_url();?>Home/AddUser">ADD</a>
	<table border="1">
		<thead>
			<th>Nama</th>
			<th>Alamat</th>
			<th>Action</th>
		</thead>
		<tbody>
			<?php foreach($query as $data){?>
			<tr>
				<td><?php echo $data['NAMA']?></td>
				<td><?php echo $data['ALAMAT']?></td>
				<td>
					<a href="<?php echo base_url();?>Home/UpdateUser/<?php echo $data['NAMA']?>">UPDATE</a>					
				</td>
			</tr>
		<?php }?>
		</tbody>
	</table>

	<a href="<?php echo base_url();?>Home/DeleteUser/" onclick=" return Confirm();">Clear Data</a>
</body>
</html>
<script type="text/javascript">
	function Confirm()
	{
		return confirm("Are you sure to clear data?");
	}
</script>